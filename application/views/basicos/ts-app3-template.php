<div class="card card-block" *ngFor="let obj of objecto">
    <h2 class="card-title">{{obj.titulo}}</h2>
    <p class="card-text" [hidden]="obj.hide">{{obj.mensaje}}</p>
    <button class="btn btn-primary" [ngClass]="{'btn-primary': obj.hide, 'btn-danger': !obj.hide}" (click)="toggle(obj)">{{obj.hide ? 'Mostrar' : 'Ocultar'}}</button>
</div>